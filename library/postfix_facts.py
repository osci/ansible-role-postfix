#!/usr/bin/python
# coding: utf-8 -*-

# (c) 2016, Michael Scherer <mscherer@redhat.com>
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.


from ansible.module_utils.basic import AnsibleModule


DOCUMENTATION = """
---
module: postfix_facts
short_description: REturn postfix configuration
version_added:
options:
"""
# TODO
EXAMPLES = """
- name: Gather information about a postfix server
  postfix_facts:

- name: Display the daemon directory, useful to place transports
  debug: msg="Daemon directory is {{ postfix_config['daemon_directory'] }}"
"""

RETURN = """
postfix_config:
    description: A dict describing the postfix config as return by postconf
    type: dict
    returned: always
postfix_version:
    description: Postfix version, in 3 dict keys major, minor, micro
    type: dict
    returned: always
"""


def generate_no_split_list(config_keys):
    # those are custom picked configuration that
    # contains string that need to be kept as is
    no_split = [
        "smtpd_banner",
        "debugger_command",
        "default_rbl_reply",
        "milter_macro_v",
    ]
    # that's a set of configuration who all share the same
    # structure. We do not hardcode them in case a new one is
    # added later
    for k in config_keys:
        if k.startswith("milter_") and k.endswith("_macros"):
            no_split.append(k)
        if k.endswith("_expansion_filter"):
            no_split.append(k)
    return no_split


def split_value(value):
    if " " in value:
        value = value.split(" ")
        for k in range(len(value)):
            if value[k][-1] == ",":
                value[k] = value[k][:-1]
        return value
    elif "," in value:
        return value.split(",")


def main():
    module = AnsibleModule(
        argument_spec=dict(),
    )
    postconf = module.get_bin_path("postconf", True)
    rc, out, err = module.run_command([postconf])

    postfix_config = {}
    for line in out.splitlines():
        key, value = line.split("=", 1)
        key = key.strip()
        value = value.strip()
        postfix_config[key] = value

    no_split_list = generate_no_split_list(postfix_config.keys())

    for key in postfix_config:
        if key not in no_split_list:
            postfix_config[key] = split_value(postfix_config[key])

    version_split = postfix_config["mail_version"].split(".")
    postfix_version = {
        "major": version_split[0],
        "minor": version_split[1],
        "micro": version_split[2],
    }

    ansible_facts = {
        "postfix_config": postfix_config,
        "postfix_version": postfix_version,
    }

    module.exit_json(changed=False, ansible_facts=ansible_facts)


main()
